# Slick Spreadsheets

[![Latest Version on Packagist](https://img.shields.io/packagist/v/itul/slick-spreadsheets.svg?style=flat-square)](https://packagist.org/packages/itul/slick-spreadsheets)
[![Total Downloads](https://img.shields.io/packagist/dt/itul/slick-spreadsheets.svg?style=flat-square)](https://packagist.org/packages/itul/slick-spreadsheets)

Slick Spreadsheets is a utility to simplify common tasks for a structured data set file like csv/xls/xlsx/html.
This package allows you to convert between formats, extract data from a file as an associative array and create a file from an array of data.

# Installation

You can install the package via composer:

```bash
composer require itul/slick-spreadsheets
```

# Methods

* [generate](#generateconvertdownload-a-file)
* General Conversion
	* [toArray](#read-file-to-array)
	* [toXls](#convert-to-xlsx)
	* [toCsv](#convert-to-csv)
	* [toHtml](#convert-to-html)
* Convert To XLSX
	* [arrayToXls](#convert-array-to-xlsx)
	* [csvToXls](#convert-csv-to-xlsx)
	* [htmlToXls](#convert-html-to-xlsx)
* Convert To Csv
	* [arrayToCsv](#convert-array-to-csv)
	* [xlsToCsv](#convert-xlsx-to-csvs)
	* [htmlToCsv](#convert-html-to-csv)
* Convert To Html
	* [arrayToHtml](#convert-array-to-html)
	* [csvToHtml](#convert-csv-to-html)
	* [xlsToHtml](#convert-xlsx-to-html)
* Convert To PDF
	* [csvToPdf](#convert-csv-to-pdf)
	* [xlsToPdf](#convert-xlsx-to-pdf)
---

## <a name="generateconvertdownload-a-file"></a>Generate/Convert/Download a File


All the below examples will trigger the browser to download a file.


```php
//GENERATE CSV FROM AN ARRAY 
\SlickSpreadsheets::generate($array, '/path/to/output.csv');

//GENERATE XLSX FROM AN ARRAY
\SlickSpreadsheets::generate($array, '/path/to/output.xlsx');

//CONVERT CSV TO XLSX
\SlickSpreadsheets::generate('/path/to/example.csv', '/path/to/output.xlsx');

//CONVERT XLSX TO CSV
\SlickSpreadsheets::generate('/path/to/example.xlsx', '/path/to/output.csv');
```

---



# General Conversion

All general conversion options (`toArray`,`toXls`,`toCsv`,`toHtml`) accept an array, or a file that can be read into the structured array.

## toArray

The toArray method accepts a file path to convert to an array.

If the provided argument is just html, this package will try to convert the html table to an array.

### <a name="read-file-to-array"></a>Read File To Array Example

```php
//CONVERT A CSV INTO A PHP ARRAY
$data = \SlickSpreadsheets::toArray('/path/to/file.csv');

//CONVERT A XLSX INTO A PHP ARRAY
$data = \SlickSpreadsheets::toArray('/path/to/file.xlsx');
```


When you are not using this package in Laravel, you will need to explicitly call the fully qualified package and run the `init` static method before using any of the available methods in this package.a


```php
$data = \Itul\SlickSpreadsheets\SlickSpreadsheets::init()->toArray('/path/to/file.csv');
```

### Examples


**CSV to Array**
```php
//CSV To array example
$data = \SlickSpreadsheets::toArray('/path/to/file.csv');
```

**XLS to Array**

This method can accept both .xls and .xslx files.

```php
//XLS To array example
//EACH SHEET IN THE SPREADSHEET WILL BE RETURNED IN THE DATA ARRAY
$data = \SlickSpreadsheets::toArray('/path/to/file.xlsx');
``` 

**HTML to Array**

This method can accept both .htm and .html files or raw html.

```php
//HTML To array example
$data = \SlickSpreadsheets::toArray('/path/to/file.hml');
``` 

```php
//HTML string To array example
$data = \SlickSpreadsheets::toArray('
    <table>
        <thead>
            <th>header1</th>
            <th>header2</th>
            <th>header3</th>
        </thead>
        <tbody>
            <tr>
                <td>row 1 col 1</td>
                <td>row 1 col 2</td>
                <td>row 1 col 3</td>
            </tr>
            <tr>
                <td>row 2 col 1</td>
                <td>row 2 col 2</td>
                <td>row 2 col 3</td>
            </tr>
            <tr>
                <td>row 3 col 1</td>
                <td>row 3 col 2</td>
                <td>row 3 col 3</td>
            </tr>
        </tbody>
    </table>
');
``` 

## <a name="convert-to-csv"></a> toCsv

This method can convert to `.csv` from: `array`, `.xls`, `.xlsx`, `.htm`, `.html`, `'(string)html'`

```php
//CONVERT ARRAY TO CSV
$csvFile = \SlickSpreadsheets::toCsv(array $array);

//CONVERT XLS/XLSX TO CSV
$csvFile = \SlickSpreadsheets::toCsv('/path/to/source/file.xlsx');

//CONVERT HTML/HTM TO CSV
$csvFile = \SlickSpreadsheets::toCsv('/path/to/source/file.html');

//CONVERT HTML string to CSV
$csvFile = \SlickSpreadsheets::toCsv(`'<table>...</table>'`);
```


## <a name="convert-to-xlsx"></a> toXls

This method can convert to .xlsx from: array/htm/html/html string/csv

```php
//CONVERT ARRAY TO CSV
$csvFile = \SlickSpreadsheets::toCsv(array $array);

//CONVERT XLS/XLSX TO CSV
$csvFile = \SlickSpreadsheets::toCsv('/path/to/source/file.xlsx');

//CONVERT HTML/HTM TO CSV
$csvFile = \SlickSpreadsheets::toCsv('/path/to/source/file.html');

//CONVERT HTML string to CSV
$csvFile = \SlickSpreadsheets::toCsv(`'<table>...</table>'`);
```

## <a name="convert-to-html"></a> toHtml

All Examples below will return `$html` as a `string`.

```php
//CONVERT ARRAY TO HTML
$html = \SlickSpreadsheets::toHtml(array $array);

//CONVERT CSV to HTML
$html = \SlickSpreadsheets::toCsv('/path/to/source/file.csv');

//CONVERT XLS/XLSX TO HTML
$html = \SlickSpreadsheets::html('/path/to/source/file.xlsx');
```




---


# Convert To XLSX

## <a name="convert-array-to-xlsx"></a>Convert array to XLSX

```php
$filePath = \SlickSpreadsheets::arrayToXls($data);
```

## <a name="convert-csv-to-xlsx"></a>Convert CSV to XLSX

```php 
$filePath = \SlickSpreadhseets::csvToXls('/path/to/file.csv');
```

## <a name="convert-html-to-xlsx"></a>Convert html to XLSX

```php
//PASS A HTML TABLE STRING
$filePath = \SlickSpreadhseets::htmlToXls($html);
```

---


# Convert To CSV

## <a name="convert-array-to-csv"></a>Convert array to CSV

```php
$filePath = \SlickSpreadsheets::arrayToCsv($data);
```


## <a name="convert-xlsx-to-csvs"></a>Convert XLSX to CSVS

```php 
//WILL RETURN AN ARRAY OF CSV FILE PATHS WITH A KEY NAME THAT MATCHES THE SHEET NAME
//FILE CAN BE .xls OR .xslx
$filePaths = \SlickSpreadhseets::xlsToCsv('/path/to/file.xlsx');
```

## <a name="convert-html-to-csv"></a>Convert html to CSV

```php
//PASS A HTML TABLE STRING  
$filePath = \SlickSpreadhseets::htmlToCsv($html);
```

---


# Convert to HTML


## <a name="convert-array-to-html"></a>Convert Array to HTML


```php
//RETURNS THE HTML AS A STRING
$html = \SlickSpreadhseets::arrayToHtml($data);
```

## <a name="convert-csv-to-html"></a>Convert CSV to HTML


```php
//RETURNS THE HTML AS A STRING
$html = \SlickSpreadhseets::csvToHtml('/path/to/file.csv');
```

## <a name="convert-xlsx-to-html"></a>Convert XLSX to HTML

```php
//RETURNS THE HTML AS A STRING
//FILE CAN BE .xls OR .xlsx
$html = \SlickSpreadhseets::xlsToHtml('/path/to/file.xlsx');
```

---


# Convert to PDF

## <a name="convert-csv-to-pdf"></a>Convert CSV to PDF

```php
$filePath = \SlickSpreadsheet::csvToPdf('/path/to/file.csv');
```

## <a name="convert-xlsx-to-pdf"></a>Convert XLSX to PDF

```php
//FILE CAN BE .xls or .xlsx
$filePath = \SlickSpreadsheet::xlsToPdf('/path/to/file.xlsx');
```