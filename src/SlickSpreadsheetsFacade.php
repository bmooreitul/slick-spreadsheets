<?php

namespace Itul\SlickSpreadsheets;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\GlobalFunctions\Skeleton\SkeletonClass
 */
class SlickSpreadsheetsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'SlickSpreadsheets';
    }
}
