<?php

namespace Itul\SlickSpreadsheets;

class SlickSpreadsheets {

    public static function init(){
        return new static;
    }

//------------------------------------------------// GENERAL CONVERSION METHODS //------------------------------------------------//

    /**
        * Convert a CSV/XLS/XLSX to a php array
        * @param    string  $filename       The full path of the file to read into an array
        * @param    string  $delimiter      The character used to split into the array
        * @return   mixed                   If the file is readable this method returns the parsed $data array else it returns false
    */
    public function toArray($filePath='', $allSheets = true, $delimiter=','){

        //CHECK FOR HTML
        if(strip_tags($filePath) != $filePath || in_array(pathinfo($filePath, PATHINFO_EXTENSION), ['htm','html'])) return $this->htmlToArray($filePath);

        //CHECK FOR EXCEL FILE
        if(in_array(pathinfo($filePath, PATHINFO_EXTENSION), ['xls','xlsx'])) return $this->xlsToArray($filePath, $allSheets);

        ini_set('auto_detect_line_endings',TRUE);

        $header = NULL;
        $data   = [];
        if(($handle = fopen($filePath, 'r')) !== FALSE){
            while(($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) (!$header ? $header = $row : $data[] = array_combine($header, $row));
            fclose($handle);
        }
        return $data;
    }

    //CONVERT TO CSV
    public function toCsv($filePath, $allSheets = true){

        //CHECK FOR ARRAY
        if(is_array($filePath)) return $this->arrayToCsv($filePath);

        //CHECK FOR HTML
        if(strip_tags($filePath) != $filePath || in_array(pathinfo($filePath, PATHINFO_EXTENSION), ['htm','html'])) return $this->htmlToCsv($filePath);

        //CHECK FOR EXCEL FILE
        if(in_array(pathinfo($filePath, PATHINFO_EXTENSION), ['xls','xlsx'])) return $this->xlsToArray($filePath, $allSheets);

    }

    //CONVERT TO XLS
    public function toXls($filePath, $allSheets = true){

        //CONVERT TO ARRAY IF NEEDED
        if(!is_array($filePath)) $filePath = $this->toArray($filePath);

        //CONVERT THE ARRAY TO XLS
        if(is_array($filePath)) return $this->arrayToXls($filePath);
    }

    //CONVERT TO HTML
    public function toHtml($filePath){

        //CONVERT TO ARRAY IF NEEDED
        if(!is_array($filePath)) $filePath = $this->toArray($filePath, false);

        //CHECK FOR ARRAY
        if(is_array($filePath)) return $this->arrayToHtml($filePath);
    }

//------------------------------------------------// CONVERT TO ARRAY //------------------------------------------------//

    //CONVERT AN EXCEL FILE TO AN ARRAY
    public function xlsToArray($filePath, $allSheets = true){
        $sheets = $this->xlsToCsv($filePath);
        $res = [];
        if(!$allSheets) foreach($sheets as $sheetName => $sheetPath) return $this->toArray($sheetPath);
        foreach($sheets as $sheetName => $sheetPath) $res[$sheetName] = $this->toArray($sheetPath);
        return $res;
    }

    //CONVERT AN HTML TABLE TO AN ARRAY
    public function htmlToArray($filePath){
        
        $DOM = new \DOMDocument();
        $DOM->loadHTML(strip_tags($filePath) != $filePath ? $filePath : file_get_contents($filePath));
        
        $Header = $DOM->getElementsByTagName('th');
        $Detail = $DOM->getElementsByTagName('td');

        //PARSE THE HEADER
        foreach($Header as $NodeHeader) $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);

        //GET ROW CONTENT WITHOUT HEADER
        $i = 0;
        $j = 0;
        foreach($Detail as $sNodeDetail) {
            $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
            $i = $i + 1;
            $j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
        }
        
        //ASIGN HEADER NAMES TO ROW CONTENT
        for($i = 0; $i < count($aDataTableDetailHTML); $i++) for($j = 0; $j < count($aDataTableHeaderHTML); $j++) $aTempData[$i][$aDataTableHeaderHTML[$j]] = $aDataTableDetailHTML[$i][$j];

        return $aTempData;
    }

//------------------------------------------------// DOWNLOAD A FILE //------------------------------------------------//

    /**
     * Generates a csv from an associative array
     * @param       array   $assocDataArray     The array to convert to a csv
     * @param       string  $fileName           The filename of the csv to be exported
     * @return      This method will always return null. It will always attempt to download the buffer as a file
     */
    public function generate($assocDataArray, $fileName = 'export.csv'){

        //CHECK IF WE ARE GENERATING A CONVERSION FILE
        if(is_string($assocDataArray) && file_exists($assocDataArray)) $assocDataArray = $this->toArray($assocDataArray);

        //GENERATE THE TEMP FILE
        $tmpFile = in_array(pathinfo($fileName, PATHINFO_EXTENSION), ['xls','xlsx']) ? $this->arrayToXls($assocDataArray) : $this->arrayToCsv($assocDataArray);
        
        //SET THE HEADERS FOR DOWNLOAD
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: '.checkMimeType($fileName)->type());
        header('Content-Disposition: attachment;filename=' . pathinfo(basename($fileName), PATHINFO_FILENAME).'.'.pathinfo($tmpFile, PATHINFO_EXTENSION));
        @readfile($tmpFile);
        exit;
    }

//------------------------------------------------// CONVERT TO XLS //------------------------------------------------//

    //CONVERT AN ARRAY TO AN XLSX FILE
    public function arrayToXls($data, $options = []){

        //GENERATE A TEMPORARY CSV OR USE A FILE PATH
        $csv = is_string($data) ? $data : $this->arrayToCsv($data);

        return $this->csvToXls($csv);

        //DEFINE A TEMPORARY TARGET FOR THE OUTPUT FILE
        $target             = sys_get_temp_dir().'/'.uniqid().'.xlsx';  
        
        //SET THE CSV READER AND PARSING OPTIONS
        $reader             = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        $reader->setDelimiter(',');
        $reader->setEnclosure('"');
        $reader->setSheetIndex(0);

        return $this->_buildExcelFile($reader->load($csv), $options);
    }

    //CONVERT A CSV TO AN XLSX FILE
    public function csvToXls($filePath){

        //LOAD THE CSV AS A SPREADSHEET
        $spreadsheet    = $this->_autoSizeCells($this->_formatHtmlDecodedCells((new \PhpOffice\PhpSpreadsheet\Reader\Csv)->load($filePath)));

        //DEFINE THE OUTPUT TARGET
        $target = sys_get_temp_dir().'/'.uniqid().'.xlsx';

        //SAVE THE OUTPUT
        (new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet))->save($target);

        //SEND BACK THE TARGET PATH
        return $target;
    }

    //CONVERT AN HTML TABLE INTO AN EXCEL FILE
    public function htmlToXls($data, $options = []){

        //BUILD THE EXCEL FILE
        return $this->_buildExcelFile((new \PhpOffice\PhpSpreadsheet\Reader\Html())->loadFromString($data), $options);
    }

//------------------------------------------------// CONVERT TO CSV //------------------------------------------------//

    //CONVERT AN ARRAY TO A CSV FILE
    public function arrayToCsv($assocDataArray){

        foreach($assocDataArray as $keys){
            $header = array_keys($keys);
            break;
        }
        $file_name  = sys_get_temp_dir().'/'.uniqid().'.csv';
        $arr        = [$header];

        foreach($assocDataArray as $values) $arr[] = $values;
        $fp         = fopen($file_name, 'w+');
        fwrite($fp, $this->_formatCsvLines($arr));
        fclose($fp);
        chmod($file_name, 0755);

        return $file_name;
    }

    //CONVERT AN HTML TABLE TO A CSV FILE
    public function htmlToCsv($data, $options = []){
        $xls    = $this->htmlToXls($data, $options);
        $csvs   = $this->xlsToCsv($xls);
        return array_shift($csvs);
    }

    //CONVERT AN EXCEL FILE TO A CSV
    public function xlsToCsv($filePath){

        $extension          = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
        $res                = [];
        $reader             = $extension == 'xls' ? new \PhpOffice\PhpSpreadsheet\Reader\Xls() : new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet        = $reader->load($filePath);
        $loadedSheetNames   = $spreadsheet->getSheetNames();
        $writer             = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);

        foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
            $target = sys_get_temp_dir().'/'.uniqid().'.csv';
            $writer->setSheetIndex($sheetIndex);
            $writer->save($target);
            chmod($target, 0755);
            $res[$loadedSheetName] = $target; 
        }

        return $res;
    }


//------------------------------------------------// CONVERT TO HTML //------------------------------------------------//

    public function arrayToHtml($data){
        return $this->csvToHtml($this->arrayToCsv($data));
    }

    public function csvToHtml($filePath){

        //DEFINE SOME DEFAULTS
        $indent = str_repeat(" ", 2);
        $arr    = $this->toArray($filePath);
        $header = array_keys($arr[0]);
        $str    = "<table class=\"table slick-spreadsheets-converted\" data-slick-spreadsheets-converted-from=\"csv\">\n{$indent}<thead>\n{$indent}{$indent}<tr>\n";

        //BUILD THE HEADER
        foreach($header as $headerVal) $str .= "{$indent}{$indent}{$indent}<th>{$headerVal}</th>\n";
        $str .= "{$indent}{$indent}</tr>\n{$indent}</thead>\n{$indent}<tbody>";

        //BUILD THE BODY ROWS
        foreach($arr as $row){
            $str .= "\n{$indent}{$indent}<tr>";
            foreach($row as $cell) $str .= "\n{$indent}{$indent}{$indent}<td>".nl2br($cell)."</td>";
            $str .= "\n{$indent}{$indent}</tr>";
        }

        //CLOSE THE BODY AND TABLE
        $str .= "\n{$indent}</tbody>\n</table>";

        return $str;

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Html((new \PhpOffice\PhpSpreadsheet\Reader\Csv)->load($filePath));
        $target = sys_get_temp_dir().'/'.uniqid().'.html';
        $writer->save($target);
        $res    = file_get_contents($target);
        unlink($target);
        return $res;
    }

    public function xlsToHtml($filePath){
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Html((new \PhpOffice\PhpSpreadsheet\Reader\Xlsx)->load($filePath));
        $writer->writeAllSheets();
        $target = sys_get_temp_dir().'/'.uniqid().'.html';
        $writer->save($target);
        $res    = file_get_contents($target);
        unlink($target);
        return $res;
    }

//------------------------------------------------// CONVERT TO PDF //------------------------------------------------//

    public function xlsToPdf($filePath){
        $pages = $this->xlsToCsv($filePath);
        $page = array_shift($pages);
        return $this->csvToPdf($page);

        $spreadsheet = (new \PhpOffice\PhpSpreadsheet\Reader\Xlsx)->load($filePath);
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf($spreadsheet);
        $target = sys_get_temp_dir().'/'.uniqid().'.pdf';
        //$writer->writeAllSheets();
        $writer->save($target);
        return $target;
    }

    public function csvToPdf($filePath){
        $spreadsheet = (new \PhpOffice\PhpSpreadsheet\Reader\Csv)->load($filePath);
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf($spreadsheet);
        $target = sys_get_temp_dir().'/'.uniqid().'.pdf';
        $writer->save($target);
        return $target;
    }

    public function htmlToPdf($data){
        return $this->csvToPdf($this->htmlToCsv($data));
    }

    public function arrayToPdf($data){
        return $this->csvToPdf($this->arrayToCsv($data));
    }

    
//------------------------------------------------// PRIVATE CLASS METHODS //------------------------------------------------//

    //FORMAT AN ARRAY OF LINES FOR CSV OUTPUT
    private function _formatCsvLines($arr){
        foreach($arr as $k => $v) $arr[$k] = $this->_formatCsvLine($v);
        return implode("\n", $arr);
    }

    //FORMAT A LINE FOR CSV OUTPUT
    private function _formatCsvLine($arr) { 
        foreach($arr as $k => $v) $arr[$k] = '"'.trim(str_replace('"', '\\"', $v)).'"';
        return implode(',',$arr);
    }

    //BUILD AN EXCEL FILE FROM A SPREADSHEET
    private function _buildExcelFile($spreadsheet, $options = []){

        //DEFINE A TEMPORARY TARGET FOR THE OUTPUT FILE
        $target             = sys_get_temp_dir().'/'.uniqid().'.xlsx';  

        //FORMAT THE SPREADSHEET
        if(is_array($options) && array_key_exists('forceText', $options) && !$options['forceText']) $spreadsheet = $this->_formatCellsAsText($spreadsheet);

        $sheet = $spreadsheet->getActiveSheet();
        foreach ($sheet->getColumnIterator() as $column) $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        if(is_array($options) && isset($options['fixedHeader'])){
            if($options['fixedHeader'] === true) $options['fixedHeader'] = 'A';
            $fixedRow = '2';
            if(strlen($options['fixedHeader']) > 1) if($freezeRow = preg_replace("/[^0-9]/", "", $options['fixedHeader'])) $fixedRow = ''; 
            $sheet->freezePane("{$options['fixedHeader']}{$fixedRow}");
        } 

        //STORE THE SPREADSHEET TO THE TARGET FILE PATH
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($target);

        chmod($target, 0755);

        //CLEAN UP THE SPREADSHEET
        $spreadsheet->disconnectWorksheets();
        unset($spreadsheet);

        //SEND BACK THE TARGET FILE
        return $target;
    }

    private function _autoSizeCells($spreadsheet){

        //SET TEXT WRAPPING AND ALIGNMENT
        $spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true)->setVertical('top');

        //GET THE ACTIVE SPREADSHEET
        $sheet = $spreadsheet->getActiveSheet();

        //AUTOSIZE THE SPREADSHEET
        foreach($sheet->getColumnIterator() as $column) $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);

        return $spreadsheet;
    }

    private function _formatHtmlDecodedCells($spreadsheet){

        //LOOP THROUGH ALL CELLS AND SET THEM TO TEXT FORMAT
        foreach($spreadsheet->getActiveSheet()->getRowIterator() as $row) foreach($row->getCellIterator() as $cell) $cell->setValue(html_entity_decode($cell->getValue()));

        return $spreadsheet;
    }

    //FORMAT AN EXCEL SHEET CELL AS TEXT
    private function _formatCellsAsText($spreadsheet, $decodeHtml = false){

        //LOOP THROUGH ALL CELLS AND SET THEM TO TEXT FORMAT
        foreach($spreadsheet->getActiveSheet()->getRowIterator() as $row) foreach($row->getCellIterator() as $cell){

            //GET THE VALUE OF THIS CELL
            $val = $cell->getValue();

            //FORMAT FLOAT VALUES AS TEXT
            if(is_float($val)) $val = strval((int)$val);

            if($decodeHtml) $val = html_entity_decode($val);
            
            //SET THE CELL FORMAT TO TEXT               
            $cell->setValueExplicit($val,  \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        }

        return $spreadsheet;
    }   
}